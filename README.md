# MemInfo

##### _Note: This information is also available in the `usage_mem_info` script_

MemInfo is a small Extension providing some basic functionality to monitor memory usage of the GMS2 Game's process.

**Disclaimer:** The memory values you see don't match what YYG's debugger shows. I'm using Windows OS functions to examine the process, and report the usage _as Windows_ sees it. My value is slightly _less_ than the TaskManager because of some additional Kernel Memory usage I'm not capturing.


### Getting Memory

*   `memInfo_get_mem_usage()`     [identical: `memInfo_get_private_wset()`]
*   `memInfo_get_working_set()`   [identical: `memInfo_get_physical_mem_usage()`]
*   `memInfo_get_private_usage()` [identical: `memInfo_get_virtual_mem_usage()`]

The Private Working Set value is _as close_ as I could find to what TaskMgr gives us. There's still some windows kernel overhead that isn't accounted for, because it's owned by the kernel. (_Note: It takes a second from boot to start getting accurate_).

The Working Set is all the memory our process is using in addition to the amount of memory shared with other processes. Shared memory is memory used for things like shared DLLs (think the C++ Runtimes you have filling up your Add/Remove programs)

The 'private' is the virtual memory your process uses, it's similar to the private working set but is missing things like the executable image

### Additional Functions

MemInfo polls at a default of every 1500 milliseconds (or 1.5 seconds). Once you call a `get_mem_xxx` function, it begins polling,but until you do, it doesn't load or poll at all, so shouldn't have nearly any overhead besides the DLL load.

* `memInfo_adjust_poll_frequency(freqInMS)`
* `freqInMS = memInfo_get_poll_frequency()`

These functions allow you to change the interval we poll at, or see what it is currently set to.

##### _Note: A minimum of 200ms is enforced in the DLL. Values below this are ignored._

### STOPPING the POLL

* memInfo_stopPolling(); 

Call this to kill the memInfo object in the DLL, and stop polling, effectively reverted back to the unloaded state.

##### _Note: calling any `get_mem_usage` fucntion will start it back up again._

