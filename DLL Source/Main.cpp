#ifdef WIN32
#include <Windows.h>
#endif 

#include <atomic>
#include <thread>
#include <stdio.h>
#include <psapi.h>
#include <memory>

#define DLL_API __declspec(dllexport)

namespace {
    // Store the values in atomics for querying from GML
    std::atomic<double> PhysMemUsage     = 0.0;
    std::atomic<double> VMemUsage        = 0.0;
    std::atomic<double> PrivateMemUsage  = 0.0;

    std::atomic<int>    PollFrequency = 1500;  // How long, in MS, to sleep
    std::atomic<bool>   ShuttingDown  = false;    
    std::atomic<bool>   Shutdown      = true;
}

// Encapsulate the game window
class CMemInfo {
private:
    DWORD                          mProcessId = 0;
    PSAPI_WORKING_SET_INFORMATION* mpPv = nullptr;
    BYTE*                          mMemBuffer = nullptr;
    size_t                         mCb = sizeof(PSAPI_WORKING_SET_INFORMATION);
    SYSTEM_INFO                    mSi;

    void FreePV() {
        if (mMemBuffer != nullptr)
            delete[] mMemBuffer;
        
        mMemBuffer = nullptr;
        mpPv = nullptr;
    }

    void GetMemInfo() {
        auto getEntries = [this]() {
            auto& _pv(*mpPv);
            uint32_t totalPrivate(0u);
            MEMORY_BASIC_INFORMATION mbi;
            
            for (uint32_t i = 0; i < _pv.NumberOfEntries; ++i) {
                auto set(_pv.WorkingSetInfo[i]);
                std::memset(&mbi, 0, sizeof(MEMORY_BASIC_INFORMATION));
                VirtualQuery(reinterpret_cast<LPCVOID>(mSi.dwPageSize * set.VirtualPage), &mbi, sizeof(MEMORY_BASIC_INFORMATION));
                if (mbi.State == MEM_COMMIT && mbi.Type == MEM_PRIVATE)
                    totalPrivate += static_cast<uint32_t>(mSi.dwPageSize);
            }
            
            PrivateMemUsage = static_cast<double>(totalPrivate);
        };
        
        if (mpPv != nullptr && QueryWorkingSet(GetCurrentProcess(), mpPv, mCb))
            getEntries();
        else {
            PSAPI_WORKING_SET_INFORMATION pv;
            std::memset(&pv, 0, sizeof(PSAPI_WORKING_SET_INFORMATION));
            if (!QueryWorkingSet(GetCurrentProcess(), &pv, sizeof(pv)) && GetLastError() == ERROR_BAD_LENGTH) {
                if (mpPv != nullptr) {
                    FreePV();
                }

                mCb = sizeof(PSAPI_WORKING_SET_INFORMATION) + ((pv.NumberOfEntries - 1) * sizeof(PSAPI_WORKING_SET_BLOCK));
                mMemBuffer = new BYTE[mCb];
                mpPv = reinterpret_cast<PSAPI_WORKING_SET_INFORMATION*>(mMemBuffer);

                if (QueryWorkingSet(GetCurrentProcess(), mpPv, mCb))
                    getEntries();
            }
        }

        PROCESS_MEMORY_COUNTERS_EX pmc;
        if (GetProcessMemoryInfo(GetCurrentProcess(), reinterpret_cast<PROCESS_MEMORY_COUNTERS*>(&pmc), sizeof(pmc)))
        {
            PhysMemUsage = static_cast<double>(pmc.WorkingSetSize);
            VMemUsage    = static_cast<double>(pmc.PrivateUsage);
            //pmc.PageFaultCount
            //pmc.PeakWorkingSetSize
            //pmc.WorkingSetSize
            //pmc.QuotaPeakPagedPoolUsage
            //pmc.QuotaPagedPoolUsage
            //pmc.QuotaPeakNonPagedPoolUsage
            //pmc.QuotaNonPagedPoolUsage
            //pmc.PagefileUsage
            //pmc.PeakPagefileUsage
        }
    }

public:

	CMemInfo()
        : mProcessId(GetCurrentProcessId())
	{
        Shutdown = false;
        std::thread([this] {
            std::memset(&mSi, 0, sizeof(SYSTEM_INFO));
            GetSystemInfo(&mSi);
            while (!ShuttingDown) {
                GetMemInfo();
                
                // Don't sleep too long if we haven't got our data yet.
                if (PrivateMemUsage == 0.0)
                    std::this_thread::yield();
                else
                    std::this_thread::sleep_for(std::chrono::milliseconds(max(PollFrequency.load(), 200)));
            }
            Shutdown = true;
        }).detach();
	}

    ~CMemInfo() {
        ShuttingDown = true;
        while (!Shutdown)
            std::this_thread::yield();

        FreePV();
        PhysMemUsage    = 0.0;
        VMemUsage       = 0.0;
        PrivateMemUsage = 0.0;
    }
};

static std::unique_ptr<CMemInfo> memInfo;
static void EnsureMemInfo() {
    if (memInfo != nullptr)
        return;
    
    memInfo = std::make_unique<CMemInfo>();
}

extern "C" {
    DLL_API double AdjustPollFrequency(double _ms) {
        PollFrequency = static_cast<int>(_ms);
        return 1.0;
    }

    DLL_API double GetPollFrequencey() {
        return static_cast<double>(PollFrequency.load());
    }

    DLL_API double GetPrivateWorkingSetMemUsage() {
        EnsureMemInfo();
        return PrivateMemUsage;
    }

    DLL_API double GetPhysicalMemUsage() {
        EnsureMemInfo();
        return PhysMemUsage;
    }

    DLL_API double GetVMemUsage() {
        EnsureMemInfo();
        return VMemUsage;
    }
    
    DLL_API double StopMemInfo() {
        memInfo.reset();
        return 0.0;
    }
}