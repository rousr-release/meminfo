///@desc using_mem_info - describe how meminfo works


// the following functions can print out the various memory amounts.
show_debug_message("Memory Usage: ");
show_debug_message("\tPrivate Working Set (task manager, almost): " + string(memInfo_get_mem_usage()));
show_debug_message("\tWorking Set (physical memory):              " + string(memInfo_get_working_set()));
show_debug_message("\tPrivate (virtual memory):                   " + string(memInfo_get_private_usage()));

/////
// What this all means:

// it should be noted that the Private Working Set value is _as close_ as I could find to what TaskMgr gives us.
// there's still some windows kernel overhead that isn't accounted for, because it's owned by the kernel.
// ... it also takes a second from boot to start getting accurate.

// the Working Set is all the memory our process is using in addition to the amount of memory shared with other processes.
// shared memory is memory used for things like shared DLLs (think the C++ Runtimes you have filling up your Add/Remove programs)

// the 'private' is the virtual memory your process uses, it's similar to the private working set but is missing things like the executable image

/////
// Additional functions

// memInfo polls at a default of every 1500 milliseconds (or 1.5 seconds.) Once you call a `get_mem_xxx` function, it begins polling,
// but until you do, it doesn't load or poll at all, so shouldn't have nearly any overhead besides the DLL load.


// memInfo_adjust_poll_frequency(5000);                          // call to update to a 5 second poll internval
// var pollInternalMilliseconds = memInfo_get_poll_frequency();  // get the current poll frequency.


//////
// STOPPING the POLL
// memInfo_stopPolling(); // call this to kill the memInfo object in the DLL, and stop polling. c

// note: calling "get_mem_usage" will start it back up again.
