///@desc use_more_memory - demo function to use more memory!
with (global.memoryHolder) {
  var arrayEnd = array_length_1d(UsedMemory);
  var newMemory = [ ];
  for (var i = 0; i < 10000; ++i)
    newMemory[i] = ds_list_create();
  
  var newScroller = instance_create_layer(0, 0, "Scrollers", RousrScroller);
  var numScrollers = array_length_1d(ScrollerLines)
 
  if (numScrollers == 0) {
    newScroller.x = newScroller.Spacing;
    newScroller.y = -newScroller.Height; 
  } else {
    var lastScroller = ScrollerLines[numScrollers - 1];
    newScroller.x = lastScroller.x - newScroller.Spacing;
    newScroller.y = lastScroller.y - newScroller.Height;
  }
 
  ScrollerLines[numScrollers] = newScroller;
  UsedMemory[arrayEnd] = newMemory;
}