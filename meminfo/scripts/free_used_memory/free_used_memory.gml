///@desc free_used_memory - demo function to free used memory!
with (global.memoryHolder) {
  var arrayEnd = array_length_1d(UsedMemory);
  for (var arrayIndex = 0; arrayIndex < arrayEnd; ++arrayIndex) {
    var mem = UsedMemory[arrayIndex];
    for (var i = 0; i < 10000; ++i)
      ds_list_destroy(mem[i]);
  }
  var numScrollers = array_length_1d(ScrollerLines);
  for (var i = 0; i < numScrollers; ++i)
    instance_destroy(ScrollerLines[i]);
  UsedMemory = [ ];
  ScrollerLines  = [ ];
}