///@desc Convert the RGB to BGR
///@param hexColor
///https://forum.yoyogames.com/index.php?threads/why-are-hex-colours-bbggrr-instead-of-rrggbb.16325/#post-105309
var c = argument0;
return (c & $FF) << 16 | (c & $FF00) | (c & $FF0000) >> 16;