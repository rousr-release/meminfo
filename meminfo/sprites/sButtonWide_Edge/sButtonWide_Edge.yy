{
    "id": "0c7dad94-d4ba-4070-af67-1fe4a370dd3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Edge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "90190d7c-6b1c-4f93-9dc1-b13f8fc032fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c7dad94-d4ba-4070-af67-1fe4a370dd3c",
            "compositeImage": {
                "id": "48243ceb-1ab3-47b0-9511-ab0a83328df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90190d7c-6b1c-4f93-9dc1-b13f8fc032fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dfe9717-cc06-4c9c-aed3-7202df7fa9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90190d7c-6b1c-4f93-9dc1-b13f8fc032fa",
                    "LayerId": "9d3f1e9f-c236-48ff-8f58-ce4e4b2bd246"
                }
            ]
        },
        {
            "id": "c81c8136-51cd-4049-84f6-d18a35025f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c7dad94-d4ba-4070-af67-1fe4a370dd3c",
            "compositeImage": {
                "id": "4b02aa98-6f91-4a78-b22b-a7bb7de8372c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c81c8136-51cd-4049-84f6-d18a35025f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54fb9ea8-cfe7-4fb0-ba01-6cb765edc83c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c81c8136-51cd-4049-84f6-d18a35025f04",
                    "LayerId": "9d3f1e9f-c236-48ff-8f58-ce4e4b2bd246"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "9d3f1e9f-c236-48ff-8f58-ce4e4b2bd246",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c7dad94-d4ba-4070-af67-1fe4a370dd3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}