{
    "id": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGenericButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d89a1523-50c0-4e07-94e6-f107bfb3cb95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "compositeImage": {
                "id": "7fc50e63-165e-46b7-a93b-03b77e69df63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d89a1523-50c0-4e07-94e6-f107bfb3cb95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8462c10e-55c4-4e07-897d-1f44599d653e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d89a1523-50c0-4e07-94e6-f107bfb3cb95",
                    "LayerId": "3b1587db-5e87-4029-a6ce-91a0b5818142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3b1587db-5e87-4029-a6ce-91a0b5818142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}