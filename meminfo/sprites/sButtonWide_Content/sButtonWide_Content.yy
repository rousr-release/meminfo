{
    "id": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonWide_Content",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "af3d739c-654a-4479-a06f-bcc56aa1fe4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "14e95c79-3537-4740-97f2-b87628d1d029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3d739c-654a-4479-a06f-bcc56aa1fe4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cdf0366-ceda-4c1e-9195-98cbf1a57161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3d739c-654a-4479-a06f-bcc56aa1fe4e",
                    "LayerId": "dd0943d7-06f7-4e46-8dc1-8f87ef1d9578"
                }
            ]
        },
        {
            "id": "816d1ea4-84d9-4886-a3d3-c86b13368ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "compositeImage": {
                "id": "aa052e9f-fe80-497d-8b97-63de845f0a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "816d1ea4-84d9-4886-a3d3-c86b13368ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c845d48f-90c0-4258-a54e-f4aeed045cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816d1ea4-84d9-4886-a3d3-c86b13368ab9",
                    "LayerId": "dd0943d7-06f7-4e46-8dc1-8f87ef1d9578"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "dd0943d7-06f7-4e46-8dc1-8f87ef1d9578",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}