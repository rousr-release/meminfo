{
    "id": "067668f5-3a07-481a-9d0f-9d18f6ffc5b5",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_rousr",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Museo Sans Cyrl",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "be0414f4-d83f-455e-b405-215c86b3b98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 114
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e895bc60-e8a0-4102-a044-0c9fb98a8b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 155,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3ae750b6-bfbc-4f48-b3c4-06cc18893191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 132,
                "y": 114
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2f50cd19-8a39-4f7a-82eb-c167c5eebdb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f3a5854e-6650-4a60-866c-fff6bffc89e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 212,
                "y": 30
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2a41fd48-4604-4b09-83aa-47aa9f43633a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c242c78e-1ff3-4a44-a485-5a9c233f533b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 197,
                "y": 30
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a2c03afa-5212-409f-9b01-29eb645ffa0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 160,
                "y": 114
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6c3834a8-5878-47a6-9d39-afe7f30f59a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 114
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ef35dd84-6962-4984-bdc4-8a8c608f01e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 114
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6d5a5494-5496-4005-a1d6-3549ef1a38ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 61,
                "y": 114
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "aa2ed356-fa00-4948-8e35-58c9eae2d2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3b9be846-6d4f-40bc-988b-001fbcb04b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 114
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b93b20b0-f643-44cc-9069-8a6bf5bea730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 114
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9575bc3e-b215-4d78-b61d-c12909b17082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 145,
                "y": 114
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "688401a0-8ac3-47f4-8f09-6909ad0a1d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 162,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "86ee399a-c7b3-427c-a585-c425357782b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 86
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6cdfd935-35d7-43ce-bdfc-b083d0b6b990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 186,
                "y": 86
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5872d2c2-2e15-4085-b03c-4fdf956320fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 86
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a34f23c7-e7a1-4fd5-9923-93eaa8c34c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 85,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b268164d-d5d5-446a-870a-1e6824b5ecdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 107,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "92c239ee-b128-4042-a41b-b509b028683e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 86
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5f6bdf8d-601f-4fd0-9944-4c2beeac1816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 137,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9242b06a-822c-4a61-bfa3-a79e87cb4996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 163,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a7ca88f1-2317-4654-bf61-3264d4ee5cc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 202,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4de5c854-88a8-4110-ba4b-a3f7b28ccb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 228,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "05a208a8-530e-4e71-9e12-f640c0b5f9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 150,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "40ecb778-e40c-442b-8b61-316883af59c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 115,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a6fdbcb2-cc54-4147-b0a2-e05693834286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e914a774-5f03-4902-8214-38412ee1837f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 173,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7bfec2b8-cb78-44be-9240-c3e8438be6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "82adbc7e-7584-48c0-93b4-ac7649f39f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "03f7d560-954d-4106-b906-f1e52e07e1bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b95e0c8f-f07c-477b-8e02-b54aad0c2027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "87439dda-072f-4d36-925a-48e85781363a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "18d73ed4-67cf-41ba-af8f-9a7c5dcd5e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "360f2b14-0f48-4e2c-88ac-1f000bcaee6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c76b62b6-e1f2-46a3-830a-88cdce196ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 241,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "67c56593-9b3e-4bb0-9dd6-8259bc78cee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 103,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b1a7fcb8-ba77-4fec-beeb-15c0ad29f81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 30
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b288b7cf-2dea-4193-8588-efa35d290b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 167,
                "y": 30
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "dd2ef9c7-a61f-43f6-82d1-9efa1f4cc362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 121,
                "y": 114
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3d95db6e-948a-41b0-949a-c233c0ef9b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 79,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4895b89c-0193-4d8a-9c4e-e8bc24bad2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 58
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6c50989e-f818-4025-b2bc-c95c1ee0facf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 91,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1582d736-c032-43af-86c7-f07f37271c07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4fef3120-db18-44de-9f33-5b009d81b55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3e8a9d5d-6ff9-4aed-8b6b-0b5225e9e91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "36f1a8ce-9763-46cb-8dbb-e1c2681ecf9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0a44a52b-b9a4-4ab3-8ec7-0e9f658756b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "45f7599c-5585-4428-9d8a-ed151b4e2280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 239,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ce938396-0539-463a-babb-5e223b5e5070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ce02ffed-cfc6-4e33-ae82-983540672917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 122,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6a812c9b-a971-48eb-b0bf-9dec6eebb60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fb44efd1-d3cd-4f73-97cd-ac93c55bfb68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7369d652-f2c6-401b-b264-1bdc9545c354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6661de53-77e2-4778-8760-2b71a247382f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 137,
                "y": 30
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "adffd2b5-eff2-48cb-84f7-fef5375435d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 152,
                "y": 30
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fb0f74a2-155a-45ce-aef9-f1fbea4446e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 58
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "147a5ef9-5a7c-432a-8a74-97721dc41953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 114
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "427bbe0b-db1c-45c7-bf99-fbb658e448b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 151,
                "y": 86
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "51e98d61-b40e-4ed9-b853-a3c9fcc3e19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 109,
                "y": 114
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "72a672ec-e48a-467b-96be-48c71b701c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 232,
                "y": 86
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "da4de743-1900-4423-a72c-838f20dd217f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e0069ed3-0575-47c6-8686-60795227e026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 5,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 165,
                "y": 114
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ca719fae-d8c0-41a4-a906-5e1350b97cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 67,
                "y": 86
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3e4352a8-15c9-4383-b3c0-196564786dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 189,
                "y": 58
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b65c09c6-174e-4e60-a45b-2672b6cef54e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 86
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f297115c-24eb-4084-a35e-0e77782af854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 225,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "74da2d1e-f5d4-4aa9-af41-10edc9942b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 72,
                "y": 58
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "414e698d-6cca-4897-81b5-69d3acc5e6a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 19,
                "y": 114
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b4e66f9c-bfe0-4cd9-896f-50a0bac00e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f06b3f0e-47a9-4d00-a56f-3f984134903d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 215,
                "y": 58
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3c22822e-7151-4e59-90b6-1f32b316dac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 140,
                "y": 114
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d5175996-a2ad-4ab2-be4e-5b5827cada53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 11,
                "y": 114
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e16845c1-f9ff-4cb2-9280-a30d01730860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 127,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "527a8c62-98d0-43c8-a57e-0d86a7a7fdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 114
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9226c230-2962-4589-8f92-ed75aeba76c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0d5db13a-06b4-4160-9dc7-144cdc7cb95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "819b0065-694d-41c7-a1bc-8c7b91e653b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 182,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "22cd71ff-3be4-4a52-9f76-a661d5073612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 66,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "62260bb6-387b-4f3f-92c7-8225a8bf00e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0c833de9-a9b3-4439-a900-3e7a63ffa702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 37,
                "y": 114
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2d88d40f-66f8-49dd-9c38-6a8f01ad518c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 197,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4f142d5d-8987-408c-8266-dd61e7acba4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 28,
                "y": 114
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b8b70364-8862-4e88-8b5a-a4a823674ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 176,
                "y": 58
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "98aa8402-af8d-4f65-af84-18d4cb11c2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 150,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a19b2dbe-57d2-4882-b761-4c22e61a7b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a74fa3a1-02fb-4260-85be-e247d5e515cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 111,
                "y": 58
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "75106a2c-3391-496e-b52e-ee511db41d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0b76293d-b3cf-498e-a787-c22bb3a9fb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 58
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "60929c63-eb87-4729-97ae-ebab0c87118d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 245,
                "y": 86
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f47237f6-9089-45a1-8ec9-9102a59d7815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 114
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "61ad8b23-1c15-4bf0-9cfb-49dcb40f6596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "85f0f7b4-6525-4085-8471-01f155d76796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 86
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "500",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}