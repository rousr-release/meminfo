{
    "id": "1a064697-ad7b-4e00-b972-d3cbe0876b1a",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "memInfo",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 64,
    "date": "2017-07-25 06:06:13",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "91895d1d-ef29-4950-9065-57e4af323fbf",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 64,
            "filename": "MemInfo.dll",
            "final": "memInfo_stopPolling",
            "functions": [
                {
                    "id": "0678f59f-7e10-480e-abe7-3d180b4eafa2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetPrivateWorkingSetMemUsage",
                    "help": "(Identical to get_private_wset) return the \"private working set\" memory usage. this won't match TaskMgr exactly because we can't compute the kernel overhead. updated at the poll frequency interval",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_mem_usage",
                    "returnType": 2
                },
                {
                    "id": "b45e0e89-c9d4-4c59-afc3-90b29cd18180",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetPollFrequencey",
                    "help": "return the current poll frequency value",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_poll_frequency",
                    "returnType": 2
                },
                {
                    "id": "b244e329-a810-448e-ab6d-63ca2db30cc4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "AdjustPollFrequency",
                    "help": "adjust how often we poll memory usage (in milliseconds) NOTE: minimum value is 200",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_adjust_poll_frequency",
                    "returnType": 2
                },
                {
                    "id": "29c8d9b2-8961-4908-9cdc-c727df5103b8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "StopMemInfo",
                    "help": "Final Function: Kill memusage thread, and stop polling mem",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_stopPolling",
                    "returnType": 2
                },
                {
                    "id": "804958e5-9ef3-4e64-8b11-225bf8edda49",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetPhysicalMemUsage",
                    "help": "This is the all of the memory, included shared memory, of the process. Shared memory consists of DLLs shared with other loaded process, etc as well as all memory used by this process",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_physical_mem_usage",
                    "returnType": 2
                },
                {
                    "id": "e50a59d9-88ed-44fd-ac4a-b2e0ebd92bb1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetPhysicalMemUsage",
                    "help": "(Identical to get_working_set) This is the all of the memory, included shared memory, of the process. Shared memory consists of DLLs shared with other loaded process, etc as well as all memory used by this process",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_working_set",
                    "returnType": 2
                },
                {
                    "id": "93f5cf73-a4c6-4d1c-8a8d-0c42fee0e428",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetPrivateWorkingSetMemUsage",
                    "help": "return the \"private working set\" memory usage. this won't match TaskMgr exactly because we can't compute the kernel overhead. updated at the poll frequency interval",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_private_wset",
                    "returnType": 2
                },
                {
                    "id": "2074da59-6460-4936-ae8d-10fc03a80614",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetVMemUsage",
                    "help": "(identical to get_private_usage) Returns the \"private\" mem usage value, or virtual memory usage",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_virtual_mem_usage",
                    "returnType": 2
                },
                {
                    "id": "536f378c-afee-4fe5-9246-b0b836e3514c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetVMemUsage",
                    "help": "Returns the \"private\" mem usage value, or virtual memory usage",
                    "hidden": false,
                    "kind": 1,
                    "name": "memInfo_get_private_usage",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                "b45e0e89-c9d4-4c59-afc3-90b29cd18180",
                "b244e329-a810-448e-ab6d-63ca2db30cc4",
                "29c8d9b2-8961-4908-9cdc-c727df5103b8",
                "0678f59f-7e10-480e-abe7-3d180b4eafa2",
                "e50a59d9-88ed-44fd-ac4a-b2e0ebd92bb1",
                "93f5cf73-a4c6-4d1c-8a8d-0c42fee0e428",
                "536f378c-afee-4fe5-9246-b0b836e3514c",
                "804958e5-9ef3-4e64-8b11-225bf8edda49",
                "2074da59-6460-4936-ae8d-10fc03a80614"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.0.0"
}