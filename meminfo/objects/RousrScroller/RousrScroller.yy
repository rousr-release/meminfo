{
    "id": "7660e8d8-def9-4b99-9dae-1e2452288675",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "RousrScroller",
    "eventList": [
        {
            "id": "be977ab6-5e9d-48b8-b142-8db976bfdc0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7660e8d8-def9-4b99-9dae-1e2452288675"
        },
        {
            "id": "d0cb9be8-6706-44b9-8bf6-934d82d08981",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7660e8d8-def9-4b99-9dae-1e2452288675"
        },
        {
            "id": "72cb41b5-0481-495f-947e-2e5dddf1967a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7660e8d8-def9-4b99-9dae-1e2452288675"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6beb9238-46f8-426a-bbb3-e1da075d2e85",
    "visible": true
}