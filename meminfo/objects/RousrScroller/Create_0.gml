/// @description RousrScroller - Create
image_xscale = 0.2;
image_yscale = 0.2;
image_alpha  = 0.8;
Spacing = 125;
VSpacing = 100;
Cols = floor(room_width / (sprite_width + 125)) + 20; // (pad the scroller count for the angles) 
Height = sprite_height + VSpacing;