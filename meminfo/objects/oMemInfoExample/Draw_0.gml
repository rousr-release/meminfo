/// @description oMemInfoExample - Draw
var textY = 10;
var fontHeight = string_height("Mem Used") + 5;
draw_set_color(colorHex($B26EF3));
draw_text(10, textY, "Mem Used (Private Working Set): " 
                     + string(memInfo_get_mem_usage() / 1024) + "K"); textY += fontHeight;
draw_text(10, textY, "Working Set (physical memory):  " 
                     + string(memInfo_get_working_set() / 1024) + "K"); textY += fontHeight;
draw_text(10, textY, "Private (virtual memory):                " 
                     + string(memInfo_get_private_usage() / 1024) + "K"); textY += fontHeight;
