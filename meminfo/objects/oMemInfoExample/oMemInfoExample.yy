{
    "id": "65c0d0d0-b519-4270-8c70-675812b42b35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMemInfoExample",
    "eventList": [
        {
            "id": "898c0244-9770-42f0-9a95-94bb4320107f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "65c0d0d0-b519-4270-8c70-675812b42b35"
        },
        {
            "id": "088936ca-30f6-46f8-9f95-db4e4c3406ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "65c0d0d0-b519-4270-8c70-675812b42b35"
        },
        {
            "id": "94d970db-5b50-4856-ac0d-a21b5c46c081",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "65c0d0d0-b519-4270-8c70-675812b42b35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}