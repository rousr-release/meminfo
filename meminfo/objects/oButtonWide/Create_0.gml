///@desc oButtonWide - Create
event_inherited();
clickAction = noone;
//clickCheck = noone;
enum TextAlign {
  Left,
  Right,
  Center
};

Label = "Generate";
LabelAlign = TextAlign.Center;
LabelPaddingX = 2;
ScaleToText = true;

ColorUp   = c_white;
ColorDown = c_gray;
UseOutlineText = false;

LeftSide  = sButtonWide_Edge;
Content   = sButtonWide_Content;
RightSide = sButtonWide_Edge;