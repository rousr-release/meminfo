///@desc oButtonWide - Draw

var labelWidth = string_width(Label);
if (ScaleToText) 
  image_xscale = labelWidth + (2 * LabelPaddingX);

var centerWidth = image_xscale;
var centerX = x;
var lx = x;
var ly = y;

if (LeftSide != undefined) {
  centerWidth -= sprite_get_width(LeftSide);
  centerX += sprite_get_width(LeftSide);
  draw_sprite(LeftSide, image_index, x, y);  
}

if (RightSide != undefined) {
  centerWidth -= sprite_get_width(RightSide);
  draw_sprite(RightSide, image_index, centerX + centerWidth, y);
}

draw_sprite_ext(Content, image_index, centerX, y, centerWidth / sprite_get_width(Content), image_yscale, 0, c_white, 1);

switch(LabelAlign) {
  case TextAlign.Left:   x = centerX; break;
  case TextAlign.Right:  x = centerX + (centerWidth - labelWidth); break;
  case TextAlign.Center: x = centerX + floor((centerWidth - labelWidth) * 0.5); break;
}

y += 1 + image_index;

draw_set_color(image_index == 0 ? ColorUp : ColorDown);
draw_text(x, y, Label);

x = lx;
y = ly;
