{
    "id": "50d9fc5e-025d-4816-b86d-666666b37c87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonWide",
    "eventList": [
        {
            "id": "e3f2e5e2-5c0c-46e8-b10b-033ba1ef6c14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "50d9fc5e-025d-4816-b86d-666666b37c87"
        },
        {
            "id": "598b1ca8-85cc-46f2-97e2-a6e5176c1f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50d9fc5e-025d-4816-b86d-666666b37c87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "eff29bc9-4f21-4617-b722-bb8a3210a527",
    "visible": true
}