{
    "id": "cf63e51f-de51-42fe-942e-3b4ec2569db8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButton",
    "eventList": [
        {
            "id": "14a559a4-274b-4892-b1c2-41b304218bbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "059ac2f1-1732-4926-bb0c-960557a4ae17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "68c7dd42-31bb-4775-a4a8-9cba4b3418ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "eeb258ed-1fb7-427f-b6b3-22eac40f15ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "3d30470c-66e2-4a07-8853-aa090d876132",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        },
        {
            "id": "3d71469b-4001-4ea5-8c2e-551476ba76f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "cf63e51f-de51-42fe-942e-3b4ec2569db8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "89d6e979-aa49-4180-8eee-a90675cc95e9",
    "visible": true
}